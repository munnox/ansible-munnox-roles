Role Name
=========

Install and setup KVM and qemu on a machine.

Requirements
------------

None as of yet

Role Variables
--------------

Description of the settable variables are in defaults/main.yml.

Dependencies
------------

None as of yet

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yaml
- hosts: all
  roles:
    - role: munnox.home.hypervisor_kvm
      state: present
      with_tcp_listen: false
```

Inventory

```yaml
all:
  hosts:
    test:
      ansible_host: hypervisior_test
      ansible_user: ubuntu
```

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
