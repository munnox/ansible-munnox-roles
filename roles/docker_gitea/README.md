Docker_gitea
=========

Install and remove gitea docker container service system from a machine. It can also backup
and restore a gitea service from zip file if given for a machine.

It currently tries to manage the full life cycle of the service. Also cleaning up on the host if asked to be absent.

Requirements
------------

```
ansible-galaxy collection install community.general
ansible-galaxy collection install community.docker
```

Role Variables
--------------

Information added as comments to defaults/main.yml

Main notable variables is the name of the archive file as default its:

```
archive_name: "prefix-{{ inventory_hostname }}.zip"
```

Dependencies
------------

```
ansible-galaxy collection install community.general
ansible-galaxy collection install community.docker
```

Also uses the `munnox.home.common` role tasks list `get_user_ids.yml`

Example Playbook and Inventory
------------------------------

Playbook

```yaml
# Add gitea role
- hosts: all
  roles:
    - import_role:
      name: munnox.home.docker_gitea
```

Inventory

```yaml
all:
  hosts:
    gitea.example.com:
      state: present
      backup: no
      restore: no
      domain_name: "{{ inventory_hostname }}"
      web_port: 3000
      container_tag: "1.13.3"
```

Task debugging

```yaml
# Add gitea role
- hosts: all
  roles:
    # - import_role:
    #   name: munnox.home.common
    # - import_role:
    #   name: munnox.home.docker
    #   state: present
  tasks:
    - name: Install Gitea
      import_role:
        name: munnox.home.docker_gitea
      vars:
        state: "present"
        backup: no
        restore: no
        domain_name: "{{ inventory_hostname }}"
        web_port: 3000
        container_tag: "1.13.3"
```

to backup file:

`inventory.yml`

```yaml
---
# Further information https://docs.ansible.com/ansible/latest/plugins/inventory/yaml.html
#
all:
  hosts:
    gitea.munnox.com:
      ansible_hostname: gitea.munnox.com
      ansible_host: 10.20.30.8
      ansible_user: ubuntu
      state: present
      backup: yes
      restore: no 
      # Default archive name
      # archive_name: "backup_{{ inventory_hostname }}.zip"
      # Custom name
      # archive_name: "gitea-dump-2021_03_07_20_08_47.zip"
      domain_name: "{{ inventory_hostname }}"
      web_port: 3000
      container_tag: "1.13.3"
```

Restore from backup file:

`inventory.yml`

```yaml
---
# Further information https://docs.ansible.com/ansible/latest/plugins/inventory/yaml.html
#
all:
  hosts:
    gitea.munnox.com:
      ansible_hostname: gitea.munnox.com
      ansible_host: 10.20.30.8
      ansible_user: ubuntu
      state: present
      backup: no
      restore: yes
      # archive_name: "backup_{{ inventory_hostname }}.zip"
      archive_name: "gitea-dump-2021_03_07_20_08_47.zip"
      domain_name: "{{ inventory_hostname }}"
      web_port: 3000
      container_tag: "1.13.3"
```


License
-------

GPLv3

Author Information
------------------

A Work in progress...
