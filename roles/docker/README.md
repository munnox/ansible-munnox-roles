Role Name
=========

Install and setup docker on a machine.

Further information:


* https://docs.docker.com/config/daemon/

Requirements
------------

None as of yet

Role Variables
--------------

Description of the settable variables are in defaults/main.yml.

Dependencies
------------

None as of yet

Example Playbook
----------------

Install docker:

```
- hosts: servers
  roles:
      - { role: docker, state: present, username: ubuntu }
```

```
- hosts: docker_hosts
  # gather_facts: yes
  # gather_subset: "min"
  roles:
    - import_role:
      name: munnox.home.docker
      state: present
      username: ubuntu
```

Uninstall docker:

```
- hosts: servers
  roles:
      - { role: docker, state: absent }
```

Inventory

```
---
all:
  hosts:
  children:
    docker_hosts:
      hosts:
        node01:
        node02:
```

License
-------

BSD

Author Information
------------------

Robert Munnoch
