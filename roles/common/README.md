Role Name
=========

Common task files groups here for inclusion else where

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------


    - hosts: servers
      tasks:
        - name: update package manager
          import_role:
            name: munnox.home.common
            tasks_from: update_package_manager.yml

    - hosts: debug
      roles:
        - import_role:
          name: munnox.home.common
          state: "{{ testing | default('not defined') }}"
          port: "{{ ansible_port }}"
          message: |
            testing: {{ testing }}
            {{ ansible_port }}
            {{ state }}

    - hosts: debug
      tasks:
        - name: get memory
          import_role:
            name: munnox.home.common
            tasks_from: get_memory.yml

    - hosts: debug
      tasks:
        # List file in directory for debugging from a molecule run
        - name: Run common/tasks/list_files.yml instead of 'main'
          ansible.builtin.import_role:
            name: common
            tasks_from: list_files.yml
          vars:
            dir: "{{ container_restore_directory }}/"
          tags: debug_service

License
-------

GPLv3

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
