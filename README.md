# Ansible Collection - munnox.home

This is a rought set of roles broken out of a internal repo being built but these roles are better and can be share to make some of the builds easier.

```bash
ansible-galaxy collection install git@gitlab:ansible_collections/munnox_home.git
```

Main roles tested are:

* `munnox.home.common`
    
    Common task lists and debug role.

    [readme](./roles/common/README.md)

* `munnox.home.hypervisor_kvm`

    Install and setup KVM as a hypervisor.
    
    [readme](./roles/hypervisor_kvm/README.md)

* `munnox.home.docker`

    Install and setup Docker as a container service.

    [readme](./roles/docker/README.md)

* `munnox.home.docker_elk`

    Install and setup elk as a docker container service
    
    [readme](./roles/docker_gitea/README.md)


* `munnox.home.docker_gitea`

    Install and setup gitea as a docker container service with backup and restore as part of the role.
    Tested up to 1.15.1
    
    [readme](./roles/docker_gitea/README.md)


* Source https://docs.ansible.com/ansible/latest/dev_guide/developing_collections.html#

There is also a molecule test role called `molecule-test`.
This is perform basic testing and assessment of the molecule testing framework.

## Installation using ansible galaxy

Install using ansible galaxy from a git repo in `ansible>=2.10` using:

```
ansible-galaxy collection install git@munnoxtech.com:robert/home_collection -p ./path_to_collections
```

or using `requirements.yml` with `ansible-galaxy collection install -r requirements.yml`.

```yaml
---
collections:
  - name: git@munnoxtech.com:ansible_collections/munnox_home.git
    scm: git
    version: master
```

## Development

```
pipenv install --dev

pipenv shell
```